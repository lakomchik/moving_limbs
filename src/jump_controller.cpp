#include "ros/ros.h"

#include <stdio.h>

#include <stdlib.h>

#include <string>

#include <std_msgs/Int16.h>

#include <std_msgs/Float32.h>

#include <vector>

using namespace std;
double timer = 1.0;

void jump_timer_callback(const std_msgs::Float32::ConstPtr & input) {
  timer = input -> data;

}

int main(int argc, char ** argv) {

  ros::init(argc, argv, "jump_controller");
  ros::NodeHandle n;
  ros::Publisher change_pos = n.advertise < std_msgs::Int16 > ("jump_mode", 1);
  ros::Subscriber sub = n.subscribe("/jump_timer", 1, jump_timer_callback);

  double begin = ros::Time::now().toSec();
  bool fl = false;
  std_msgs::Int16 mode;

  ros::Rate loop_rate(1000);
  while (ros::ok()) {

    double now = ros::Time::now().toSec();
    if (now - begin >= timer) {
      if (fl == true) {
        mode.data = 0;
        fl = false;

      } else {
        mode.data = 1;
        fl = true;
      }
      begin = now;

      change_pos.publish(mode);
    }

    ros::spinOnce();

    loop_rate.sleep();

  }

  return 0;

}