#include "ros/ros.h"

#include <stdio.h>

#include <stdlib.h>

#include <string>

#include <math.h>

#include <nav_msgs/Odometry.h>

#include "unitree_legged_msgs/LowCmd.h"

#include "unitree_legged_msgs/LowState.h"

#include "unitree_legged_msgs/MotorCmd.h"

#include "unitree_legged_msgs/MotorState.h"

#include <geometry_msgs/WrenchStamped.h>

#include <sensor_msgs/Imu.h>

#include <std_msgs/Bool.h>

#include <std_msgs/Int16.h>

#include <vector>

using namespace std;

std_msgs::Int16 jump_mode;
unitree_legged_msgs::MotorCmd motor_message[12];
unitree_legged_msgs::LowState lowState;
int mode;

void paramInit() {
  for (int i = 0; i < 12; i++) {
    motor_message[i].mode = 10;
    motor_message[i].dq = 0.5;
    motor_message[i].tau = 0.0;
    motor_message[i].Kp = 70.0;
    motor_message[i].Kd = 8.0;
    motor_message[i].reserve[0] = 0;
    motor_message[i].reserve[1] = 0;
    motor_message[i].reserve[2] = 0;
  }

}

void up() {
  double pos[12] = {
    0.0,
    0.67,
    -1.3,
    -0.0,
    0.67,
    -1.3,
    0.0,
    0.67,
    -1.3,
    -0.0,
    0.67,
    -1.3
  };
  for (int i = 0; i < 12; i++) {
    motor_message[i].q = pos[i];
  }

}

void down() {
  double pos[12] = {
    0.0,
    1.38,
    -2.58,
    0.0,
    1.38,
    -2.58,
    0.0,
    1.38,
    -2.58,
    0.0,
    1.38,
    -2.58
  };
  for (int i = 0; i < 12; i++) {
    motor_message[i].q = pos[i];
  }

}

void jump_callback(const std_msgs::Int16::ConstPtr & input) {
  mode = input -> data;
  if (mode == 0) {
    up();
  }
  if (mode == 1) {
    down();
  }
}

int main(int argc, char ** argv) {
  ros::init(argc, argv, "kinematics");
  ros::NodeHandle n;

  string robot_name;
  ros::param::get("/robot_name", robot_name);
  cout << "robot_name: " << robot_name << endl;
  ros::Publisher servo_pub[12]; {

  }
  servo_pub[0] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/FR_hip_controller/command", 1);
  servo_pub[1] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/FR_thigh_controller/command", 1);
  servo_pub[2] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/FR_calf_controller/command", 1);
  servo_pub[3] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/FL_hip_controller/command", 1);
  servo_pub[4] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/FL_thigh_controller/command", 1);
  servo_pub[5] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/FL_calf_controller/command", 1);
  servo_pub[6] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/RR_hip_controller/command", 1);
  servo_pub[7] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/RR_thigh_controller/command", 1);
  servo_pub[8] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/RR_calf_controller/command", 1);
  servo_pub[9] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/RL_hip_controller/command", 1);
  servo_pub[10] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/RL_thigh_controller/command", 1);
  servo_pub[11] = n.advertise < unitree_legged_msgs::MotorCmd > ("/" + robot_name + "_gazebo/RL_calf_controller/command", 1);
  ros::Subscriber sub = n.subscribe("/jump_mode", 1, jump_callback);
  paramInit();
  up();
  ros::Rate loop_rate(100);
  while (ros::ok()) {

    for (int i = 0; i < 12; i++) {
      servo_pub[i].publish(motor_message[i]);

    }

    ros::spinOnce();

    loop_rate.sleep();

  }

  return 0;

}