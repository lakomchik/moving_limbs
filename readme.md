# **Пакет, заставляющий ~~отжиматься~~ прыгать  Unitree A1 в Gazebo**
## Необходимо для работы:
#### [Unitree_ros](https://github.com/unitreerobotics/unitree_ros)
## Как запустить:
#### $ roslaunch moving_limbs jumping.launch
## Как поменять период отжиманий:
#### $ rostopic pub /jump_timer std_msgs/Float32 "data: $(your_period)" 
